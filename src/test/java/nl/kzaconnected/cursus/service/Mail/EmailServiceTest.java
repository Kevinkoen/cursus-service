package nl.kzaconnected.cursus.service.Mail;


import nl.kzaconnected.cursus.model.Dao.Cursist;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import nl.kzaconnected.cursus.service.EmailService;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Rule
    public SmtpServerRule smtpServerRule = new SmtpServerRule(2525);

    @Value("${spring.mail.mainemailadres}")
    private String mainEmailAdres;

    @Test
    public void shouldSendMail() throws MessagingException, IOException {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("test@mail.nl").build();
        String returnMsg = "";

        try {
            returnMsg = emailService.sendMail(cursus, cursist);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals("Mail is succesvol verzonden.", returnMsg);

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(2, receivedMessages.length);

        MimeMessage currentMail = receivedMessages[0];
        assertEquals("Aanmelding cursus", currentMail.getSubject());
        assertEquals(mainEmailAdres, currentMail.getAllRecipients()[0].toString());
        assertEquals(cursist.getEmail(), currentMail.getAllRecipients()[1].toString());
        assertTrue(String.valueOf(currentMail.getContent()).contains("Hallo Chantal"));
        assertTrue(String.valueOf(currentMail.getContent()).contains("Graag wil ik mij aanmelden voor de volgende cursus:"));
        assertTrue(String.valueOf(currentMail.getContent()).contains(cursus));
        assertTrue(String.valueOf(currentMail.getContent()).contains("Gr.,"));
        assertTrue(String.valueOf(currentMail.getContent()).contains(cursist.getNaam()));

        // Check if cc is same as to
        assertEquals(currentMail.getSubject(), receivedMessages[1].getSubject());
        assertEquals(currentMail.getAllRecipients(), receivedMessages[1].getAllRecipients());
        assertEquals(currentMail.getContent(), receivedMessages[1].getContent());
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsInvalid() throws MessagingException, IOException {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("invalid@.nl").build();

        try {
            emailService.sendMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsEmptyString() throws MessagingException, IOException {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("").build();

        try {
            emailService.sendMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsNull() throws MessagingException, IOException {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").build();

        try {
            emailService.sendMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }
}
